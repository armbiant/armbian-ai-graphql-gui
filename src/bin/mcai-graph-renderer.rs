use clap::Parser;

use mcai_graph::{GraphConfiguration, NodeConfiguration};
use mcai_graph_renderer::{
  Output, OutputFormat, SvgOutput, WorkflowRenderer, WorkflowRendererOptions,
};

#[derive(Parser, Debug)]
#[clap(version)]
struct Args {
  /// Path to the workflow JSON file
  #[clap(short, long, name("input-file"))]
  input_file: String,

  /// Output format
  #[clap(short, long, value_enum)]
  format: OutputFormat,

  /// Path to the outputfile
  #[clap(short, long, name("output-file"))]
  output_file: String,

  /// Width of the nodes
  #[clap(short, long, default_value_t = 300)]
  width: usize,

  /// Height of the nodes
  #[clap(short, long, default_value_t = 50)]
  height: usize,

  /// X gap of the nodes
  #[clap(short, long, default_value_t = 50, name("x-gap"))]
  x_gap: usize,

  /// Y gap of the nodes
  #[clap(short, long, default_value_t = 50, name("y-gap"))]
  y_gap: usize,
}

fn main() -> Result<(), mcai_graph_renderer::error::Error> {
  let args = Args::parse();

  let renderer = WorkflowRenderer::load_workflow(
    &args.input_file,
    GraphConfiguration::new(NodeConfiguration::new(
      args.width,
      args.height,
      args.x_gap,
      args.y_gap,
    )),
  )?;

  match args.format {
    OutputFormat::SVG => {
      let svg_output = SvgOutput {}.render(renderer, WorkflowRendererOptions::default())?;
      std::fs::write(args.output_file, svg_output)?;

      Ok(())
    }
  }
}
