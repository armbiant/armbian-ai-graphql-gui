pub mod svg;

use crate::error::Result;

use mcai_graph::{Graph, GraphConfiguration, LinkType, ToGraph};
use mcai_models::{Workflow, WorkflowDefinition};

#[derive(clap::ValueEnum, Clone, Debug)]
pub enum OutputFormat {
  SVG,
}

#[derive(Clone)]
pub struct WorkflowRendererOptions {
  link_type: LinkType,
  link_color: String,
  link_width: String,
  nodes_border_color: String,
  nodes_fill_color: String,
  icon_fill_color: String,
}

impl Default for WorkflowRendererOptions {
  fn default() -> Self {
    Self {
      link_type: LinkType::Parentage,
      link_color: "#696f74".to_string(),
      link_width: "2px".to_string(),
      nodes_border_color: "#cacaca".to_string(),
      nodes_fill_color: "white".to_string(),
      icon_fill_color: "#696f74".to_string(),
    }
  }
}

pub struct WorkflowRenderer {
  workflow: Workflow,
  graph: Graph,
}

impl WorkflowRenderer {
  pub fn load_workflow(workflow_path: &str, configuration: GraphConfiguration) -> Result<Self> {
    let str_workflow_definition = std::fs::read_to_string(workflow_path)?;
    let workflow_definition_json: WorkflowDefinition =
      serde_json::from_str(&str_workflow_definition)?;

    let workflow: Workflow = Workflow::Definition(workflow_definition_json);
    let graph = workflow.to_graph(configuration);

    Ok(Self { workflow, graph })
  }
}

pub trait Output {
  fn render(
    &self,
    workflow_render: WorkflowRenderer,
    render_options: WorkflowRendererOptions,
  ) -> Result<Vec<u8>>;
}
