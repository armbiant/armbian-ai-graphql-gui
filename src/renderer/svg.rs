use crate::{error::Result, Output, WorkflowRenderer, WorkflowRendererOptions};

use mcai_graph::{Link, SharedNode};
use mcai_models::Step;
use mcai_types::Coordinates;
use std::collections::HashMap;
use svg::{
  node::{
    self,
    element::{path::Data, Group, Path, Rectangle, Text},
  },
  Document,
};

const SVG_MARGIN: isize = 10;
pub struct SvgOutput;

impl SvgOutput {
  fn render_icon_as_svg(
    icon_name: &str,
    coord: Coordinates,
    options: &WorkflowRendererOptions,
  ) -> Result<Group> {
    let icons: HashMap<&str, &str> = include!(concat!(env!("OUT_DIR"), "/icons.rs"));

    let data = icons
      .get(&icon_name)
      .unwrap_or_else(|| icons.get("settings").unwrap());

    let icon_path = Path::new()
      .set("stroke", options.icon_fill_color.clone())
      .set("d", *data);

    let mut group = Group::new().set(
      "transform",
      format!("translate({}, {})", coord.x + 10, coord.y),
    );

    group = group.add(icon_path);

    Ok(group)
  }

  fn render_links(
    document: &mut Document,
    links: Vec<Link>,
    options: &WorkflowRendererOptions,
  ) -> Result<()> {
    links.iter().for_each(|link| {
      let data = Data::new()
        .move_to((link.start().x, link.start().y))
        .cubic_curve_to((
          link.start().x,
          link.start().y - 40,
          link.end().x,
          link.end().y + 40,
          link.end().x,
          link.end().y,
        ));

      let svg_link = Path::new()
        .set("fill", "transparent")
        .set("stroke", options.link_color.clone())
        .set("stroke-width", options.link_width.clone())
        .set("d", data);

      *document = document.clone().add(svg_link);
    });

    Ok(())
  }

  fn render_nodes(
    document: &mut Document,
    nodes: HashMap<u32, SharedNode>,
    steps: &[Step],
    options: &WorkflowRendererOptions,
  ) -> Result<(isize, isize)> {
    let mut viewbox_x = 0;
    let mut viewbox_y = 0;

    nodes.iter().for_each(|(id, node)| {
      let coord = node.borrow().coordinates();
      let dimensions = node.borrow().dimensions();
      let step = steps[*id as usize].clone();

      viewbox_x = if coord.x + dimensions.width() as isize > viewbox_x {
        coord.x + dimensions.width() as isize
      } else {
        viewbox_x
      };
      viewbox_y = if coord.y + dimensions.height() as isize > viewbox_y {
        coord.y + dimensions.height() as isize
      } else {
        viewbox_y
      };

      let icon_svg =
        Self::render_icon_as_svg(&step.icon.to_string(), coord.clone(), options).unwrap();

      let label = Text::new()
        .add(node::Text::new(step.label))
        .set("x", coord.x as usize + dimensions.width() / 2)
        .set("y", coord.y as usize + dimensions.height() / 2)
        .set("dominant-baseline", "middle")
        .set("text-anchor", "middle");

      let rectangle = Rectangle::new()
        .set("x", coord.x)
        .set("y", coord.y)
        .set("width", dimensions.width())
        .set("height", dimensions.height())
        .set("fill", options.nodes_fill_color.clone())
        .set("stroke", options.nodes_border_color.clone())
        .set("stroke-width", "2");

      let step_svg = Group::new()
        .set("x", coord.x)
        .set("y", coord.y)
        .set("width", dimensions.width())
        .set("height", dimensions.height())
        .add(rectangle)
        .add(icon_svg)
        .add(label);

      *document = document.clone().add(step_svg);
    });
    Ok((viewbox_x, viewbox_y))
  }
}

impl Output for SvgOutput {
  fn render(
    &self,
    workflow_render: WorkflowRenderer,
    render_options: WorkflowRendererOptions,
  ) -> Result<Vec<u8>> {
    let mut document = Document::new();

    Self::render_links(
      &mut document,
      workflow_render.graph.get_links(render_options.link_type),
      &render_options,
    )?;

    let (viewbox_x, viewbox_y) = Self::render_nodes(
      &mut document,
      workflow_render.graph.nodes(),
      workflow_render.workflow.steps(),
      &render_options,
    )?;

    document = document.set(
      "viewBox",
      (
        -SVG_MARGIN,
        -SVG_MARGIN,
        viewbox_x + 2 * SVG_MARGIN,
        viewbox_y + 2 * SVG_MARGIN,
      ),
    );

    Ok(document.to_string().into_bytes())
  }
}
