# MCAI Graph renderer

Library to automate MCAI worfklow graph generation.

## Supported output format

As of now, the crate supports the following formats:

- [x] SVG

## Arguments

The repo includes a binary that can be installed and started with the following arguments:

|argument|short|required|description|
|-|-|-|-|
|--input_file|-i|true|Path to the workflow JSON file.|
|--output_file|-o|true|Path to the output file.|
|--format|-f|true|Format of the output graph.|
|--width||false|Width of the nodes on the graph (default: 300).|
|--height||false|Height of the nodes on the graph (default: 300).|
|--x_gap||false|Horizontal space between nodes (default: 50).|
|--y_gap||false|Vertical space between nodes (default: 50).|
|--help||false|Display help.|
|--version|-V|false|Version number.|

## Example usage

```shell
mcai-workflow-renderer -f "/path/to/workflow.json" --format svg -o generated_graph.svg
```
